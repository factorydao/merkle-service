const projects = [
  {
    key: "fvt",
    label: "finance.vote",
    logo: "finance.svg",
    symbol: "FVT",
    address: {
      1: "0x45080a6531d671DDFf20DB42f93792a489685e32",
      3: "0x32a3F75A1EDF6F70951C2A01A9A614b9bA3E2AE1",
    },
  },
  {
    key: "cudos",
    label: "Cudos",
    logo: "DL_airdrop.png",
    symbol: "CUDOS",
    address: {
      1: "0x817bbDbC3e8A1204f3691d14bB44992841e3dB35",
    },
  },
  {
    key: "totm",
    label: "Totem",
    logo: "DL_airdrop.png",
    symbol: "TOTM",
    address: {
      1: "0x6FF1BFa14A57594a5874B37ff6AC5efbD9F9599A",
    },
  },
  {
    key: "bump",
    label: "BUMP",
    logo: "DL_airdrop.png",
    symbol: "BUMP",
    address: {
      1: "0x785c34312dfA6B74F6f1829f79ADe39042222168",
    },
  },
  {
    key: "hid",
    label: "Hypersign Identity",
    logo: "DL_airdrop.png",
    symbol: "HID",
    address: {
      1: "0xB14eBF566511B9e6002bB286016AB2497B9b9c9D",
    },
  },
  {
    key: "l3p",
    label: "Lepricon",
    logo: "DL_airdrop.png",
    symbol: "L3P",
    address: {
      1: "0xdeF1da03061DDd2A5Ef6c59220C135dec623116d",
    },
  },
  {
    key: "metis",
    label: "Metis",
    logo: "DL_airdrop.png",
    symbol: "METIS",
    address: {
      1: "0x9E32b13ce7f2E80A01932B42553652E053D6ed8e",
    },
  },
  {
    key: "oly",
    label: "Olyseum",
    logo: "oly-logo.svg",
    symbol: "OLY",
    address: {},
  },
];

module.exports = {
  async up(db, client) {
    for (const project of projects) {
      await db
        .collection("projects")
        .findOneAndUpdate(
          { key: project.key },
          { $set: project },
          { upsert: true }
        );
    }
    const merkleTrees = await db.collection("merkletrees").find().toArray();
    for (const tree of merkleTrees) {
      const project = await db.collection("projects").findOne({
        $or: [
          { key: tree.additionalData.project },
          { _id: tree.additionalData.project },
        ],
      });
      await db
        .collection("merkletrees")
        .findOneAndUpdate(
          { _id: tree._id },
          { $set: { "additionalData.project": project._id } },
          { upsert: true }
        );
    }
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  },
};
