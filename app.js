const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const { subscribeToRootCreation } = require("./helpers/subscribe");
const { startCleanerService } = require("./helpers/cleaner");
const { rateLimiter, treeLimiter } = require("./helpers/security");
const indexRouter = require("./routes/index");

const app = express();

app.use(logger("dev"));
app.use(express.json({ limit: "200mb" }));
app.use(express.urlencoded({ extended: true, limit: "200mb" }));
app.use(cookieParser());
app.use(cors());
app.use(rateLimiter);
app.use('/create-merkle-tree/', treeLimiter);

app.use("/", indexRouter);

subscribeToRootCreation();
startCleanerService();

module.exports = app;
