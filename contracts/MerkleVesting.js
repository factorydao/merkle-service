const MerkleVesting = {
  abi: [
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "index",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "newRoot",
          type: "bytes32",
        },
      ],
      name: "MerkleRootAdded",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "destination",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "numTokens",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "tokensLeft",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "uint256",
          name: "merkleIndex",
          type: "uint256",
        },
      ],
      name: "WithdrawalOccurred",
      type: "event",
    },
    {
      inputs: [
        {
          internalType: "bytes32",
          name: "rootHash",
          type: "bytes32",
        },
        {
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "tokenBalance",
          type: "uint256",
        },
      ],
      name: "addMerkleRoot",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "numTree",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "value",
          type: "uint256",
        },
      ],
      name: "depositTokens",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "merkleIndex",
          type: "uint256",
        },
        {
          internalType: "address",
          name: "destination",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "totalCoins",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "startTime",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "endTime",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "lockPeriodEndTime",
          type: "uint256",
        },
        {
          internalType: "bytes32[]",
          name: "proof",
          type: "bytes32[]",
        },
      ],
      name: "initialize",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      name: "initialized",
      outputs: [
        {
          internalType: "bool",
          name: "",
          type: "bool",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      name: "merkleTrees",
      outputs: [
        {
          internalType: "bytes32",
          name: "rootHash",
          type: "bytes32",
        },
        {
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "tokenBalance",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "numTrees",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      name: "tranches",
      outputs: [
        {
          internalType: "uint256",
          name: "totalCoins",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "currentCoins",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "startTime",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "endTime",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "coinsPerSecond",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "lastWithdrawalTime",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "lockPeriodEndTime",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "merkleIndex",
          type: "uint256",
        },
        {
          internalType: "address",
          name: "destination",
          type: "address",
        },
      ],
      name: "withdraw",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
  ],
  contractName: "MerkleVesting",
  networks: {
    3: {
      address: "0x5f993D41F112F2C0Ba59998748e292b84d0744D4",
      fromBlock: 12149077,
    },
    4: {
      address: "0xFE218baCE3545C943D20602C17A0140407b5251c",
      fromBlock: 10577132,
    },
    97: {
      address: "0xD0706DE027380a93bD3e4f56105FC5231D5cECc4",
      fromBlock: 19436129,
    },
    43113: {
      address: "0x80ebfdc0A52Dd0d703B46C7D2ACE64C77D12d0F5",
      fromBlock: 8938055,
    },
  },
};

if (process.env.NODE_ENV === "test") {
  MerkleVesting.networks = {};
}

module.exports = MerkleVesting;
