const MerkleDropFactory = {
  contractName: "MerkleDropFactory",
  abi: [
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "index",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "newRoot",
          type: "bytes32",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
      ],
      name: "MerkleTreeAdded",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "merkleIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "recipient",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "value",
          type: "uint256",
        },
      ],
      name: "Withdraw",
      type: "event",
    },
    {
      inputs: [
        {
          internalType: "bytes32",
          name: "newRoot",
          type: "bytes32",
        },
        {
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
        {
          internalType: "address",
          name: "depositToken",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "tokenBalance",
          type: "uint256",
        },
      ],
      name: "addMerkleTree",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "numTree",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "value",
          type: "uint256",
        },
      ],
      name: "depositTokens",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      name: "merkleTrees",
      outputs: [
        {
          internalType: "bytes32",
          name: "merkleRoot",
          type: "bytes32",
        },
        {
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "tokenBalance",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "spentTokens",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "numTrees",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "merkleIndex",
          type: "uint256",
        },
        {
          internalType: "address",
          name: "walletAddress",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "value",
          type: "uint256",
        },
        {
          internalType: "bytes32[]",
          name: "proof",
          type: "bytes32[]",
        },
      ],
      name: "withdraw",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      name: "withdrawn",
      outputs: [
        {
          internalType: "bool",
          name: "",
          type: "bool",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
  ],
  networks: {
    1: {
      address: "0xB0E136eC95aec90055eA98383C513c5da1f17Bc5",
      fromBlock: 14660038,
    },
    3: {
      address: "0xB67d97eda0F93F2e0712EcC4D982c17CDE1E51Ba",
      fromBlock: 10906470,
    },
    4: {
      address: "0xC1EC61dfde2082d56E2A4D062D66aa20bfB8e2f9",
      fromBlock: 10577120,
    },
    97: {
      address: "0x5FEE4A044e48Db6Db75Dc7AE0e9E2D2aDA5418E8",
      fromBlock: 19436068,
    },
    333: {
      address: "0xCfEB869F69431e42cdB54A4F4f105C19C080A601",
      fromBlock: 100000,
    },
    43113: {
      address: "0x7Cd6AF066E32ed25eE3C971cEfBB05eD699AEa36",
      fromBlock: 8938054,
    },
  },
};

if (process.env.NODE_ENV === "test") {
  MerkleDropFactory.networks = {
  };
}

module.exports = MerkleDropFactory;
