const MerkleResistorV2 = {
  contractName: "MerkleResistor",
  abi: [
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "AccountEmpty",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "bytes32", name: "leaf", type: "bytes32" },
      ],
      name: "AlreadyInitialized",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "bytes32", name: "leaf", type: "bytes32" },
        { internalType: "bytes32[]", name: "proof", type: "bytes32[]" },
      ],
      name: "BadProof",
      type: "error",
    },
    {
      inputs: [{ internalType: "uint256", name: "treeIndex", type: "uint256" }],
      name: "BadTreeIndex",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "vestingTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "minTotalPayments",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "maxTotalPayments",
          type: "uint256",
        },
      ],
      name: "BadVestingSchedule",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "min", type: "uint256" },
        { internalType: "uint256", name: "max", type: "uint256" },
      ],
      name: "IncoherentTimes",
      type: "error",
    },
    {
      inputs: [{ internalType: "uint256", name: "pct", type: "uint256" }],
      name: "InvalidPct",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "UninitializedAccount",
      type: "error",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "newRoot",
          type: "bytes32",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
      ],
      name: "MerkleTreeAdded",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount",
          type: "uint256",
        },
      ],
      name: "TokensDeposited",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "uint256",
          name: "trancheIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "recipient",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "leaf",
          type: "bytes32",
        },
      ],
      name: "TrancheInitialized",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "destination",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "numTokens",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "tokensLeft",
          type: "uint256",
        },
      ],
      name: "WithdrawalOccurred",
      type: "event",
    },
    {
      inputs: [],
      name: "PRECISION",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "bytes32", name: "newRoot", type: "bytes32" },
        { internalType: "bytes32", name: "ipfsHash", type: "bytes32" },
        { internalType: "uint256", name: "minEndTime", type: "uint256" },
        { internalType: "uint256", name: "maxEndTime", type: "uint256" },
        { internalType: "uint256", name: "pctUpFront", type: "uint256" },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        { internalType: "uint256", name: "tokenBalance", type: "uint256" },
      ],
      name: "addMerkleTree",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "value", type: "uint256" },
      ],
      name: "depositTokens",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "bytes32", name: "leaf", type: "bytes32" },
      ],
      name: "getInitialized",
      outputs: [{ internalType: "bool", name: "", type: "bool" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "getTranche",
      outputs: [
        { internalType: "address", name: "", type: "address" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "vestingTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "minTotalPayments",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "maxTotalPayments",
          type: "uint256",
        },
        { internalType: "bytes32[]", name: "proof", type: "bytes32[]" },
      ],
      name: "initialize",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      name: "merkleTrees",
      outputs: [
        { internalType: "bytes32", name: "merkleRoot", type: "bytes32" },
        { internalType: "bytes32", name: "ipfsHash", type: "bytes32" },
        { internalType: "uint256", name: "minEndTime", type: "uint256" },
        { internalType: "uint256", name: "maxEndTime", type: "uint256" },
        { internalType: "uint256", name: "pctUpFront", type: "uint256" },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "tokenBalance",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "numTranchesInitialized",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "numTrees",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "vestingTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "minTotalPayments",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "maxTotalPayments",
          type: "uint256",
        },
      ],
      name: "verifyVestingSchedule",
      outputs: [
        { internalType: "bool", name: "", type: "bool" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "withdraw",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
  ],
  networks: {
    43113: {
      address: "0x5488cd6BE311686E7Cd434Fb4E702c8273cAaa99",
      fromBlock: 16725836,
    },
    5: {
      address: "0xc24D65f494Dd62042Fd5704DFa59FE92b3aaC850",
      fromBlock: 7744212,
    },
  },
};

if (process.env.NODE_ENV === "test") {
  MerkleResistorV2.networks = {
    // 4: {
    //   address: "0xE4C3736122109393f234BcAb500110022FF152Fa",
    //   fromBlock: 11084593,
    // },
    5: {
      address: "0xc24D65f494Dd62042Fd5704DFa59FE92b3aaC850",
      fromBlock: 7744212,
    },
  };
}

module.exports = MerkleResistorV2;
