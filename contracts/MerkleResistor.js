const MerkleResistor = {
  contractName: "MerkleResistor",
  abi: [
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "index",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "newRoot",
          type: "bytes32",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
      ],
      name: "MerkleTreeAdded",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "index",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount",
          type: "uint256",
        },
      ],
      name: "TokensDeposited",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "destination",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "numTokens",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "tokensLeft",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "uint256",
          name: "merkleIndex",
          type: "uint256",
        },
      ],
      name: "WithdrawalOccurred",
      type: "event",
    },
    {
      inputs: [],
      name: "PRECISION",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "bytes32", name: "newRoot", type: "bytes32" },
        { internalType: "bytes32", name: "ipfsHash", type: "bytes32" },
        { internalType: "uint256", name: "minEndTime", type: "uint256" },
        { internalType: "uint256", name: "maxEndTime", type: "uint256" },
        { internalType: "uint256", name: "pctUpFront", type: "uint256" },
        {
          internalType: "address",
          name: "depositToken",
          type: "address",
        },
        { internalType: "uint256", name: "tokenBalance", type: "uint256" },
      ],
      name: "addMerkleTree",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "value", type: "uint256" },
      ],
      name: "depositTokens",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "merkleIndex", type: "uint256" },
        { internalType: "address", name: "destination", type: "address" },
        { internalType: "uint256", name: "vestingTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "minTotalPayments",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "maxTotalPayments",
          type: "uint256",
        },
        { internalType: "bytes32[]", name: "proof", type: "bytes32[]" },
      ],
      name: "initialize",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "address", name: "", type: "address" },
        { internalType: "uint256", name: "", type: "uint256" },
      ],
      name: "initialized",
      outputs: [{ internalType: "bool", name: "", type: "bool" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      name: "merkleTrees",
      outputs: [
        { internalType: "bytes32", name: "merkleRoot", type: "bytes32" },
        { internalType: "bytes32", name: "ipfsHash", type: "bytes32" },
        { internalType: "uint256", name: "minEndTime", type: "uint256" },
        { internalType: "uint256", name: "maxEndTime", type: "uint256" },
        { internalType: "uint256", name: "pctUpFront", type: "uint256" },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        { internalType: "uint256", name: "tokenBalance", type: "uint256" },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "numTrees",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "address", name: "", type: "address" },
        { internalType: "uint256", name: "", type: "uint256" },
      ],
      name: "tranches",
      outputs: [
        { internalType: "uint256", name: "totalCoins", type: "uint256" },
        {
          internalType: "uint256",
          name: "currentCoins",
          type: "uint256",
        },
        { internalType: "uint256", name: "startTime", type: "uint256" },
        { internalType: "uint256", name: "endTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "coinsPerSecond",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "lastWithdrawalTime",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "merkleIndex", type: "uint256" },
        { internalType: "uint256", name: "vestingTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "minTotalPayments",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "maxTotalPayments",
          type: "uint256",
        },
      ],
      name: "verifyVestingSchedule",
      outputs: [
        { internalType: "bool", name: "", type: "bool" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "merkleIndex", type: "uint256" },
        { internalType: "address", name: "destination", type: "address" },
      ],
      name: "withdraw",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
  ],
  networks: {
    1: {
      address: "0xe6B1b43A6a34b38ce0c4A3F4A2B468Eb2580CA82",
      fromBlock: 14660606,
    },
    4: {
      address: "0xb72d4A1702190D795b5c25525b0E90EE0E6E3556",
      fromBlock: 10577100,
    },
    97: {
      address: "0x6d8d286dCaFb54a38C88E86A811bBd6A48809137",
      fromBlock: 19436084,
    },
    43113: {
      address: "0x289E5cEAee04a2ce218582a4eDca7e9628B395CE",
      fromBlock: 9018130,
    },
  },
};

if (process.env.NODE_ENV === "test") {
  MerkleResistor.networks = {};
}

module.exports = MerkleResistor;
