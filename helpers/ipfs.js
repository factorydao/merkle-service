const { pinHash } = require("infura-ipfs");
const { create } = require("ipfs-http-client");
const axios = require('axios').default;

const ipfs = create({
  url: `http://${process.env.IPFS_NODE_HOST}:5001/api/v0`,
});

const pinJson = async (body) => {
  let _body;
  if (typeof body === "object") {
    _body = JSON.stringify(body);
  }
  try {
    const { cid: hashToPin } = await ipfs.add(_body);
    // await pinHash(hashToPin.toString());
    const hash = hashToPin.toString();
    await axios.post(`${process.env.PINNING_SERVICE_URL || 'http://pinning:8080'}/pin`, { hashToPin: hash }, {
      headers: {
      secret: process.env.PINNING_SECRET
    }})
    return hash;
  } catch (err) {
    console.error('Error while trying to pin', err);
    return Promise.reject(err);
  }
};
module.exports = {
  pinJson,
};
