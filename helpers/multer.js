const multer = require("multer");
const path = require("path");
const mimeTypes = [
  "application/json",
  "application/x-javascript",
  "text/javascript",
  "text/x-javascript",
  "text/x-json",
  "text/csv",
];
const fs = require("fs");

const checkImageType = (file, cb) => {
  const imgRegex = new RegExp("image/", "i");
  return file.mimetype.match(imgRegex)
    ? cb(null, true)
    : cb(new Error("Wrong mimetype"), false);
};
const checkLeavesType = (file, cb) => {
  return mimeTypes.includes(file.mimetype)
    ? cb(null, true)
    : cb(new Error("Wrong mimetype"), false);
};

const fileFilter = (req, file, cb) => {
  const filterFn =
    file.fieldname === "projectLogo" ? checkImageType : checkLeavesType;
  return filterFn(file, cb);
};
const dir = path.join(process.cwd(), "temp");
const imageDir = path.join(process.cwd(), "images");
const storage = multer.diskStorage({
  destination: async function (req, file, cb) {
    const _dir = file.fieldname === "projectLogo" ? imageDir : dir;
    try {
      await fs.promises.access(_dir);
    } catch (error) {
      await fs.promises.mkdir(_dir, { recursive: true });
    } finally {
      cb(null, _dir);
    }
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const holdersUpload = multer({ storage: storage, fileFilter: fileFilter });

module.exports = {
  holdersUpload,
};
