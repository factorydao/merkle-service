const { BigNumber } = require("ethers");

const merkleTreeData = [
  {
    destination: "0x18F96835c557a7E6d3EaA1Bca450f57BA6928f30",
    value: "9334323816728840000000000",
  },
  {
    destination: "0xd5198F31C6206E5fCAD59EbEa38d6425539FD24d",
    value: "9370920340059130000000000",
  },
  {
    destination: "0x7137f62c282E9837C91440d405eA7A912EedF5E7",
    value: "9334323816728840000000000",
  },
  {
    destination: "0x90BEd24e15661F041677cF06Bdd4C09b5334Feb3",
    value: "1120118858052270000000000",
  },
  {
    destination: "0x4EA4A38e351Ae6e4d29F19a173986A178144132f",
    value: "1829826166514180000000000",
  },
  {
    destination: "0x8fAF97b6692F3058f36d99373407aD9e573a774C",
    value: "600182982616651000000000",
  },
  {
    destination: "0xB79A09b9d10D091e04A96a368342B9139AEaC31D",
    value: "2285983370267390000000000",
  },
  {
    destination: "0x76Ac38f789645d2bf08274CbAfCf82174781fC48",
    value: "2240237716104530000000000",
  },
  {
    destination: "0x3A1E9E54F4D708E3ACBDb2CFaa3eC94f34B9b02e",
    value: "1152790484903930000000000",
  },
  {
    destination: "0x14B1DE0E7E1106e629c3abe3F09F3373C428D123",
    value: "1829826166514180000000000",
  },
  {
    destination: "0xF59c213F54AAAE2133061207fA975b4052ccFa1E",
    value: "914913083257091000000000",
  },
  {
    destination: "0xC9503d276A7004569721De5ec5D486D0B401D8ee",
    value: "927721866422690000000000",
  },
  {
    destination: "0x3ac9c5d5d98a725396ff757627736b0ef11c2b0d",
    value: "50196000000000000000000",
  },
  {
    destination: "0xa13bb257b3fc3f476ff2eaf28726363e2205a02e",
    value: "20586000000000000000000",
  },
];

describe("Merkle tree", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  test("it should calculate needed token balance for the merkle tree correctly", () => {
    const expectedResult = "41011950668169722000000000";
    const { createMerkleTree } = require("./merkle");

    const { treeBalance } = createMerkleTree(
      merkleTreeData,
      ["address", "uint256"],
      ["destination", "value"]
    );

    expect(treeBalance).toEqual(expectedResult);
  });

  test("it should calculate needed token balance for the merkle tree correctly with 3% commision", () => {
    
    const expectedResult = "42242309188214813660000000";

    process.env.COMMISSION_PERCENTAGE = 3;
    process.env.COMMISSION_ADDRESS =
      "0xeFab1eB78ffF1db84180A90964300FDA7533b186";
    const { createMerkleTree } = require("./merkle");

    const { treeBalance } = createMerkleTree(
      merkleTreeData,
      ["address", "uint256"],
      ["destination", "value"]
    );

    expect(treeBalance).toEqual(expectedResult);
  });

  test("it should calculate needed token balance for the merkle tree correctly with 5% commision", () => {
    const expectedResult = "43062548201578208100000000";

    process.env.COMMISSION_PERCENTAGE = 5;
    process.env.COMMISSION_ADDRESS =
      "0xeFab1eB78ffF1db84180A90964300FDA7533b186";
    const { createMerkleTree } = require("./merkle");

    const { treeBalance } = createMerkleTree(
      merkleTreeData,
      ["address", "uint256"],
      ["destination", "value"]
    );

    expect(treeBalance).toEqual(expectedResult);
  });
});
