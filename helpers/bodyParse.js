const bodyParse = async (req, res, next) => {
  if (req.body.data && typeof req.body.data === "string") {
    req.body = JSON.parse(req.body.data);
    return next();
  } else {
    next();
  }
};

module.exports = {
  bodyParse,
};
