const express = require("express");
const router = express.Router();
const MerkleTreeModel = require("../models/MerkleTree");
const ProjectModel = require("../models/Project");
const {
  createMerkleProof,
  createMerkleTree,
  fileToMerkleTree,
  createMerkleProofForAddress,
  contracts,
  parseLeavesValues,
  isValidLeaves,
  parseLeavesValuesFromFile,
} = require("../helpers/merkle");
const { pinJson } = require("../helpers/ipfs");
const { hasAccessMiddleware } = require("../helpers/security");
const mongoose = require("mongoose");
const { holdersUpload } = require("../helpers/multer");
const { bodyParse } = require("../helpers/bodyParse");
const { getAddress } = require("ethers/lib/utils");
const { Contract, BigNumber, utils } = require("ethers");
const {
  MERKLE_CONTRACTS,
  getProvider,
  REQUIRE_PERCENTAGE,
  fromWei,
  toWei,
  getContractTreeData,
  getDecimalPlaces,
} = require("../helpers/utils");
const { subscribeToSingleRootCreation } = require("../helpers/subscribe");
const MerkleTree = require("../models/MerkleTree");
const MerkleEvent = require("../models/MerkleEvent");
const { getProject } = require("../helpers/database");

router.post(
  "/create-merkle-tree/:project",
  holdersUpload.fields([{ name: "projectLogo", maxCount: 1 }]),
  bodyParse,
  async (req, res) => {
    try {
      let merkleTree;
      let leaves;
      const {
        msg: { inputData, additionalData, friendlyValues },
        address,
      } = req.body;

      const { projectLogo } = req.files;
      const { project } = req.params;

      if (!additionalData.project._id) {
        additionalData.project.logo = projectLogo[0]?.path;
      }

      const { depositToken, chainId, contract } = additionalData;

      const [_project, decimalPlaces] = await Promise.all([
        getProject(additionalData.project),
        getDecimalPlaces(depositToken, chainId),
      ]);

      console.log("Start tree calculating");
      const { hashTypes, hashKeys, type } = contracts[contract];

      if (inputData) {
        const isValidError = isValidLeaves(inputData, type);

        if (isValidError.length) {
          throw isValidError;
        }

        leaves =
          friendlyValues || decimalPlaces !== 18
            ? parseLeavesValues(inputData, type, friendlyValues, decimalPlaces)
            : inputData;
      } else {
        throw "Missing input data";
      }

      merkleTree = createMerkleTree(leaves, hashTypes, hashKeys);

      console.log("Tree calculated", merkleTree.root?.hash);
      const ipfsHash = await pinJson(leaves);
      const db = mongoose.connection;
      const collectionName = merkleTree.root.hash;
      const foundRootHash = await db
        .collection(collectionName)
        .find({ hash: collectionName })
        .toArray();
      if (!foundRootHash.length) {
        await Promise.all([
          db.collection(collectionName).createIndex({ index: 1 }),
          db.collection(collectionName).createIndex({ "data.destination": 1 }),
          db.collection(collectionName).createIndex({ hash: 1 }),
        ]);
        const bulk = db.collection(collectionName).initializeOrderedBulkOp();
        for (const node of merkleTree.nodes) {
          bulk.insert(node);
        }
        await bulk.execute();
        delete merkleTree.nodes;
      }
      const newMerkleTree = await MerkleTreeModel.create({
        project,
        root: merkleTree.root,
        additionalData: {
          ...additionalData,
          project: _project,
          tokenBalance: merkleTree.treeBalance,
          treeBalance: merkleTree.treeBalance,
          owner: address,
        },
      });
      //subscribeToSingleRootCreation(additionalData.chainId);
      return res.json({
        success: true,
        root: merkleTree.root.hash,
        ipfsHash,
        totalBalance: merkleTree.treeBalance,
        dbTreeId: newMerkleTree?._id?.toString(),
      });
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
);

router.post("/update-merkle-tree", bodyParse, async (req, res) => {
  try {
    const { updateBody, signature } = req.body;
    const { dbTreeId, newTreeId } = updateBody;
    const treeToUpdate = await MerkleTreeModel.findOne({ _id: dbTreeId });
    if (!treeToUpdate) throw `Tree not found ${treeToUpdate}`;

    const recovered = utils.verifyMessage(JSON.stringify(updateBody), signature);
    if (recovered !== utils.getAddress(treeToUpdate.additionalData?.owner))
      throw 'Wrong signature';

    treeToUpdate.treeIndex = newTreeId;
    await treeToUpdate.save();

    return res.json({
      success: true,
      treeId: dbTreeId,
    });
  } catch (error) {
    console.log(error);
    return res.status(400).send(error);
  }
});

router.get("/:project/check/:address", async (req, res) => {
  try {
    const { address, project } = req.params;
    const query = {
      project,
      treeIndex: { $exists: true },
      $or: [
        { treeIndex: { $exists: true } },
        {
          $and: [
            { "inputData.destination": { $regex: address, $options: "i" } },
            { treeIndex: { $exists: true } },
          ],
        },
      ],
    };
    if (Object.keys(req.query).length) {
      for (let param in req.query) {
        query[`additionalData.${param}`] = req.query[param];
      }
    }
    const trees = await MerkleTreeModel.find(query, {
      treeIndex: 1,
      additionalData: 1,
      inputData: {
        $elemMatch: { destination: { $regex: address, $options: "i" } },
      },
      root: 1,
      _id: 1,
      version: 1,
    })
      .populate({
        path: "additionalData.project",
        model: ProjectModel,
      })
      .lean();
    const treesArray = [];

    for (const tree of trees) {
      if (!tree.root) {
        treesArray.push(tree);
        continue;
      }
      const {
        treeIndex,
        _id,
        root: { hash },
        additionalData,
        version,
      } = tree;

      const onAddressList = await mongoose.connection
        .collection(hash)
        .find({ "data.destination": { $regex: address, $options: "i" } })
        .toArray();

      let events;
      if (onAddressList?.length) {
        events = await MerkleEvent.find({
          contract: tree.additionalData.contract,
          treeIndex: tree.treeIndex,
          chainId: tree.additionalData.chainId,
          owner: getAddress(address),
          version: tree.version,
        }).lean();

        // FIXME: UNCOMMENT THIS AFTER FIXING PERCENTAGE CHECK
        for (let addressLeaf of onAddressList) {
          treesArray.push({
            _id,
            treeIndex,
            additionalData,
            inputData: { hash: addressLeaf.hash, data: addressLeaf.data },
            events,
            version,
          });
        }
      }
      let isEnoughBalance = false;

      /* FIXME: UNCOMMENT THIS AFTER FIXING PERCENTAGE CHECK
      if (onAddressList?.length) {
        let neededBalance = Number(fromWei(additionalData.treeBalance));
        const requiredPercentage = Number(REQUIRE_PERCENTAGE.toString());

        let currentBalance;
        const chainId = additionalData.chainId;
        const provider = getProvider(chainId);
        switch (additionalData.contract) {
          case "MerkleResistor":
            const merkleResistor = new Contract(
              MERKLE_CONTRACTS.MerkleResistor.networks[chainId].address,
              MERKLE_CONTRACTS.MerkleResistor.abi,
              provider
            );

            const merkleResistorData = await merkleResistor.merkleTrees(
              treeIndex
            );

            currentBalance = Number(fromWei(merkleResistorData.tokenBalance));
            break;
          case "MerkleVesting":
            const merkleVesting = new Contract(
              MERKLE_CONTRACTS.MerkleVesting.networks[chainId].address,
              MERKLE_CONTRACTS.MerkleVesting.abi,
              provider
            );

            const merkleVestingData = await merkleVesting.merkleTrees(
              treeIndex
            );
            const tokenBalance = Number(
              fromWei(merkleVestingData.tokenBalance)
            );

            currentBalance = tokenBalance;
            break;

          default:
            const merkleFactory = new Contract(
              MERKLE_CONTRACTS.MerkleDropFactory.networks[chainId].address,
              MERKLE_CONTRACTS.MerkleDropFactory.abi,
              provider
            );

            const merkleFactoryData = await merkleFactory.merkleTrees(
              treeIndex
            );

            currentBalance = Number(fromWei(merkleFactoryData.tokenBalance));
        }

        isEnoughBalance =
          (currentBalance / neededBalance) * 100 >= requiredPercentage;

        if (currentBalance == 0 && neededBalance == 0) isEnoughBalance = true;

        if (isEnoughBalance)
          treesArray.push({
            _id,
            treeIndex,
            additionalData,
            inputData: onAddressList.map((item) => item.data),
            events,
          }); 
      }*/
    }
    return res.json(treesArray);
  } catch (error) {
    console.log(error);
    return res.json([]);
  }
});

/*
  router.get("/token-uri/:treeId/:tokenId", async (req, res) => {
    try {
      const { treeId, tokenId } = req.params;
      const tree = await MerkleTreeModel.findOne({ _id: treeId });
      const leaf = tree.inputData.metadataLeaves.find(
        (leaf) => leaf.tokenId == tokenId
      );
      res.json({ uri: leaf.uri });
    } catch (err) {
      console.error(err);
      res.status(400).json({ success: false });
    }
  });
*/

router.get("/proof/:treeId/:hash", async (req, res) => {
  try {
    const { hash, treeId } = req.params;
    const tree = await MerkleTreeModel.findOne({ _id: treeId });
    if (!tree)
      return res
        .status(400)
        .json({ success: false, message: "Requested tree doesn't exist" });
    let merkleProof;
    let address;
    try {
      address = getAddress(hash);
    } catch (err) {
      //
    }
    if (tree.tree) {
      merkleProof = address
        ? createMerkleProof(tree.tree, address, "destination")
        : createMerkleProof(tree.tree, hash, "");
    } else {
      if (!!address) {
        const walletAddress = getAddress(hash);
        merkleProof = await createMerkleProofForAddress(
          walletAddress,
          "destination",
          tree.root.hash
        );
      } else {
        merkleProof = await createMerkleProofForAddress(
          hash,
          "",
          tree.root.hash
        );
      }
    }
    res.json({
      merkleProof,
    });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
});

router.get("/dashboard", async (req, res) => {
  try {
    const merkleTrees = await MerkleTree.find(
      {
        treeIndex: { $exists: true },
      },
      {
        additionalData: 1,
        treeIndex: 1,
        _id: 0,
      }
    )
      .populate({
        path: "additionalData.project",
        model: "Project",
      })
      .lean();
    const promises = [];
    for (const tree of merkleTrees) {
      const { additionalData, treeIndex } = tree;
      additionalData.project = additionalData.project.label;
      promises.push(getContractTreeData(treeIndex, additionalData));
    }
    return res.json(await Promise.all(promises));
  } catch (error) {
    console.log(error);
    return res.status(400).json({});
  }
});

router.get("/projects", async (req, res, next) => {
  try {
    const projects = await ProjectModel.find({}, { logo: 0 });
    res.send(projects);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get("/logo/:projectKey", async (req, res, next) => {
  try {
    const { projectKey } = req.params;
    const key = new RegExp(projectKey, "i");
    const { logo } = await ProjectModel.findOne({ key }, { logo: 1 }).lean();
    return res.sendFile(logo);
  } catch (error) {
    console.log(error);
    return res.status(400).send("Logo not found");
  }
});

module.exports = router;
