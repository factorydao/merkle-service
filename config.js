const { env } = process;

module.exports = {
  database: {
    host: env.DB_HOST || "localhost",
    port: env.DB_PORT || "27017",
    name: env.DB_NAME || "merkle-trees",
  },
};
